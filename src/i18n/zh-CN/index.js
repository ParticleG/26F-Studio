export default {
  layout: {
    main: {
      title: '26F Studio',
      tab: {
        about: '关于',
        product: '产品列表',
        service: '游戏服务',
      },
      menu: {
        products: [
          '方块研究所',
          '适用于Love引擎的ColdClear机器人',
        ],
        services: [
          '接入TechNode传输节点',
          'TechBotServer方块机器人服务器',
        ],
      }
    }
  },
  genericFailure: '操作失败',
  genericSuccess: '操作成功',
}
