
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout'),
    children: [
      { path: '', component: () => import('pages/root/Index') },
      { path: 'products', component: () => import('pages/products/Index') },
    ]
  },
  {
    path: '/products/techmino',
    component: () => import('layouts/TechminoLayout'),
    children: [
      { path: '', component: () => import('pages/products/techmino/Index') },
    ]
  },

  {
    path: '/:catchAll(.*)*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
